;;;;;
title: Progress report #9
date: 2017-07-29 17:00
format: md
author: Daniel Kochmański
;;;;;

Dear Community,

McCLIM code is getting better on a weekly basis depending on developer
time. We are happy to see the project moving forward.

**Some highlights for this iteration**:

- Scigraph code cleanup and bug fixes,
- Bezier curves improvements,
- PostScript and PDF improvements,
- CLX-fb and mcclim-renderer speed improvements and refactor,
- various code cleanups from unused and broken constructs,
- editorial corrections to the CLIM 2 specification sources we bundle
with McCLIM

Moreover many bug fixes have been proposed and merged into the
codebase.

All McCLIM bounties (both active and already solved) may be found
[here](https://www.bountysource.com/teams/mcclim/bounties). Default
bounty expiration date is 6 months after posting it (a bounty may be
reissued after that time period).

To answer recurring requests for native Windows and OSX support, we
have posted bountes for finishing the Windows backend and fixing the
OSX backend. Moreover, to improve portability a bounty for closer-mop
support has been posted.

**Bounties solved this iteration**:

- [$100] Caps lock affects non-alphabetic keys.

**Active bounties** ($1700):

- [$500] Windows Backend (*new*).
- [$400] Fix Beagle backend (*new*).
- [$300] Replace MOP things with closer-mop portability layer (*new*).
- [$150] When flowing text in a FORMATTING-TABLE, the pane size is
  used instead of the column size.
- [$150] clx: input: english layout.
- [$100] Add PDF file generation (PDF backend).
- [$100] Keystroke accelerators may shadow keys even if inactive.

Our current financial status is $800 for bounties and $267 recurring
monthly contributions from the supporters (thanks!).

Suggestions as to which other issues should have a bounty on them are
appreciated and welcome. Please note that Bountysource has a
functionality "Suggest an Issue" which may be found on the
[bounties](https://www.bountysource.com/teams/mcclim/bounties)
page. If you would like to work on an issue that is not covered by the
existing bounties, feel free to suggest a new bounty.

If you have any questions, doubts or suggestions – please contact me
either by email (daniel@turtleware.eu) or on IRC (my nick is
`jackdaniel`).

Sincerely yours,  
Daniel Kochmański
