;;;;;
title: McCLIM 0.9.7 "Imbolc" release
date: 2018-02-16 16:00
format: md
author: Daniel Kochmański
;;;;;

After 10 years we have decided that it is time to make a new release – the first
one since 2008, which was McCLIM 0.9.6, *St. George's Day*. *Imbolc* is a Gaelic
traditional festival marking the beginning of spring held between the winter
solstice and the spring equinox.

Due to a long period of time, the number of changes is too big to list in full
detail and we will thus note only major changes made during the last eleven
iterations (though many important changes were done before that). For more
information please check out previous iteration reports
on [McCLIM blog](https://common-lisp.net/project/mcclim/1.html),
[git log](https://github.com/robert-strandh/McCLIM/commits/master) and
the [issue tracker](https://github.com/robert-strandh/McCLIM/issues). We'd like
to thank all present and past contributors for their time, support and testing.

- Bug fix: tab-layout fixes.
- Bug fix: formatting-table fixes.
- Bug fix: scrolling and viewport fixes and refactor.
- Feature: raster image draw backend extension.
- Feature: bezier curves extension.
- Feature: new tests and demos in clim-examples.
- Feature: truetype rendering is now default on clx.
- Feature: additions to region, clipping rectangles and drawing.
- Feature: clim-debugger and clim-listener improvmenets.
- Feature: mop is now done with CLOSER-MOP.
- Feature: threading is now done with BORDEAUX-THREADS.
- Feature: clx-fb backend (poc of framebuffer-based backend).
- Feature: assumption that all panes must be mirrored has been removed.
- Cleanup: many files cleaned up from style warnings and such.
- Cleanup: removal of PIXIE.
- Cleanup: removal of CLIM-FFI package.
- Cleanup: changes to directory structure and asd definitions.
- Cleanup: numerous manual additions and corrections.
- Cleanup: broken backends has been removed.
- Cleanup: goatee has been removed in favour of Drei.
- Cleanup: all methods have now corresponding generic function declarations.

We also have a bounty program financed with money from the fundraiser. We are
grateful for financial contributions which allow us to attract new developers
and reward old ones with bounties. Currently active bounties (worth $2650) are
available [here](https://www.bountysource.com/teams/mcclim/bounties).

As *Imbolc* marks the beginning of spring we hope this release will be one of
many in the upcoming future.
