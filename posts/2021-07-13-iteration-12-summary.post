;;;;;
title: Progress report #12
date: 2021-07-13 19:00
format: md
tags: clim, lisp
author: Daniel Kochmański
;;;;;

Dear Community,

A lot of time passed since the last blog entry. I'm sorry for neglecting this.
In this post, I'll try to summarize the past two and a half year.

## Finances and bounties

Some of you might have noticed that the bounty program has been suspended. The
BountySource platform lost our trust around a year ago when they have changed
their ToS to include:

> If no Solution is accepted within two years after a Bounty is posted, then the
> Bounty will be withdrawn, and the amount posted for the Bounty will be
> retained by Bountysource.

They've quickly retracted from that change, but the trust was already lost. Soon
after, I've suspended the account and all donations with this platform were
suspended. BountySource refunded all our pending bounties.

All paid bounties were summarized in previous posts. Between 2016-08-16 and
2020-06-16 (46 months of donations) we have collected in total $18700. The
Bounty Source comission was 10% collected upon withdrawal - all amounts
mentioned below are presented for **before** the comission was claimed.

During that time $3200 was paid to bounty hunters who solved various issues in
McCLIM. The bounty program was a limited success - solutions that were
contributed were important, however harder problems with bounties were not
solved. That said, a few developers who contribute today to McCLIM joined in the
meantime and that might be partially thanks to the bounty program.

When the fundraiser was announced, I've declared I would withdraw $600 monthly
from the project account. In the meantime I've had a profitable contract and for
two years I stopped withdrawing money. During the remaining three years I've
withdrawn $15500 ($440/month) from the account.

As of now we don't have funds and there is no official way to donate money to
the project (however, this may change in the near future). I hope that this
summary is sufficient regarding the fundraiser. If you have further questions,
please don't hesitate to contact me, and I'll do my best to answer them.

I'd like to thank all people who donated to the project. Your financial support
made it possible for me to work on the project more than I would be able without
it. The fact that people care about McCLIM enough to contribute to it money
gives me the motivation and faith that working on the codebase is an important
effort that benefits the Common Lisp community.

## Improvements

The last update was in 2018-12-31. A lot of changes accumulated in the meantime.

- Bordered output bug fixes and improvements -- Daniel Kochmański
- Gadget UX improvements (many of them) -- Jan Moringen
- Text styles fixes and refactor -- Daniel Kochmański
- Freetype text renderer improvements -- Elias Mårtenson
- Extended input stream abstraction rewrite -- Daniel Kochmański
- Implementation of presentation methods for dialog-views -- admich
- Encapsulating stream missing methods implementation -- admich
- indenting-output-stream fixes -- Jan Moringen
- drawing-tests demo rewrite -- José Ronquillo Rivera
- Line wrap on the word boundaries -- Daniel Kochmański
- New margin implementation (extended text formatting) -- Daniel Kochmański
- Presentation types and presentation translators refactor -- Daniel Kochmański
- Input completion and accept methods bug fixes and reports -- Howard Shrobe
- Clipboard implementation (and the selection translators) -- Daniel Kochmański
- CLIM-Fig demo improvements and bug fixes -- Christoph Keßler
- The pointer implementation (fix the specification conformance) -- admich
- Drei kill ring improvements -- Christoph Keßler
- McCLIM manual improvements -- Jan Moringen
- Frame icon and pretty name change extensions -- Jan Moringen
- Cleanups and extensive testing -- Nisar Ahmad
- pointer-tracking rewrite -- Daniel Kochmański
- drag-and-drop translators rewrite -- Daniel Kochmański
- Complete rewrite of the inspector Clouseau -- Jan Moringen
- Rewrite of the function distribute-event -- Daniel Kochmański and Jan Moringen
- Adding new tests and organizing them in modules -- Jan Moringen
- Various fixes to the delayed repaint mechanism -- Jan Moringen
- CLX backend performance and stability fixes -- Christoph Keßler
- PS/PDF/Raster backends cleanups and improvements -- Jan Moringen
- Drei regression fixes and stability improvements -- Nisar Ahmad
- Geometry module refactor and improvements -- Daniel Kochmański
- Separating McCLIM code into multiple modules -- Daniel Kochmański and Jan Moringen
- Frames and frame managers improvements -- Jan Moringen and Daniel Kochmański
- Frame reinitialization -- Jan Moringen
- PDF/PS backends functionality improvements -- admich
- Menu code cleanup -- Jan Moringen
- Pane geometry and graph formatting fixes -- Nisar Ahmad
- Numerous CLX cleanups and bug fixes -- Daniel Kochmański and Jan Moringen
- Render backend stability, performance and functionality fixes -- Jan Moringen
- Presentation types more strict interpretation -- Daniel Kochmański
- External Continuous Integration support -- Jan Moringen
- Continuous Integration support -- Nisar Ahmad
- Improved macros for recording and table formatting -- Jan Moringen
- Better option parsing for define-application-frame -- Jan Moringen
- Separation between the event queue and the stream input buffer -- Daniel Kochmański
- Examples cleanup -- Jan Moringen
- Graph formatting cleanup -- Daniel Kochmański
- Stream panes defined in define-application-frames refactor -- admich
- Menu bar rewrite (keyboard navigation, click to activate) -- Daniel Kochmański
- Thread-safe execute-frame-command function -- Daniel Kochmański
- Mirroring code simplification for clx-derived backends -- Daniel Kochmański
- Arbitrary native transformations for sheets (i.e. zooming) -- Daniel Kochmański
- extended-streams event matching improvements -- Jan Moringen
- Render backend performance improvements -- death
- drei fixes for various issues -- death
- drei various cleanups -- Jan Moringen
- clim-debugger improvements -- Jan Moringen
- Manual spelling fixes and proofreading -- contrapunctus

This is not an exhaustive list of changes. For more details, please consult the
repository history. Many changes I've introduced during this iteration were a
subject of a careful (and time-consuming) peer review from Jan Moringen which
resulted in a better code quality. Continuous integration provided by Nisar
Ahmad definitely made my life simpler. I'd like to thank all contributors for
their time and energy spent on improving McCLIM.

## Pending work

If you are working on some exciting improvement for McCLIM which is not ready,
you may make a "draft" pull request in the McCLIM repository. Currently, there
are three such branches:

- the SLIME-based [backend](https://github.com/McCLIM/McCLIM/pull/1193) for CLIM
by Luke Gorrie

- the dot-based graph layout
[extension](https://github.com/McCLIM/McCLIM/pull/1197) by Eric Timmons

- the xrender [backend](https://github.com/McCLIM/McCLIM/pull/1196) by Daniel
Kochmański

Other than that, I've recently implemented the polygon triangulation algorithm
that is meant to be used in the xrender backend (but could be reused i.e. for
opengl). Currently, I'm refining the new rendering for clx (xrender). After that,
I want to introduce a portable double buffering and a new repaint queue. Having
these things in place after extensive testing, I want to roll out a new release
of McCLIM.

Sincerely yours,  
Daniel Kochmański
