;;;;;
title: Progress report #8
date: 2017-06-12 21:00
format: md
author: Daniel Kochmański
;;;;;

Dear Community,

During this iteration we had many valuable contributions. It's a joy
to see how McCLIM gains more mindshare and people are willing to put
their time and wallet in fixing issues and writing applications in
McCLIM.

**Some highlights for this iteration**:

- many Listener fixes,
- major tab layout extension refactor,
- new extension for Bezier curves (based on older internal implementation),
- interactor improvements,
- layout improvements,
- fixes for redisplay and transformations,
- documentation cleanups,
- cleanup of the issues (closed the obsolete and fixed ones).

![Bezier Curves](https://common-lisp.net/project/mcclim/static/media/it8-cap-2017-bezier.png "Bezier curves")

All McCLIM bounties (both active and already solved) may be
found [here](https://www.bountysource.com/teams/mcclim/bounties).

**Bounties solved this iteration**:

- [$200] Interactor CLI prompt print problem

  Fixed by Gabriel Laddel. Waiting for a pull request and a bounty claim.

- [$200] Problem with coordinate swizzling (probably).

  Fixed by Alessandro Serra and merged. Waiting for a bounty claim.

- [$100] menu for input-prompt in lisp-listener does not disappear after use.

  Fixed by Alessandro Serra and merged. Waiting for a bounty claim.

**Active bounties**:

- [$150] When flowing text in a FORMATTING-TABLE, the pane size is
  used instead of the column size.

- [$150] clx: input: english layout. (*someone already works on it*).

- [$100] Caps lock affects non-alphabetic keys. (*new*)

- [$100] Add PDF file generation (PDF backend). (*new*)

- [$100] Keystroke accelerators may shadow keys even if inactive. (*new*)

Our current financial status is $1,429 for bounties and $267 recurring
monthly contributions from the supporters (thanks!).

Suggestions as to which other issues should have a bounty on them are
appreciated and welcome. Please note that Bountysource has a
functionality "Suggest an Issue" which may be found on
the [bounties](https://www.bountysource.com/teams/mcclim/bounties)
page. If you feel that you may solve some problem, but there is no
bounty on it, feel free to suggest it too.

If you have any questions, doubts or suggestions – please contact me
either by email (daniel@turtleware.eu) or on IRC (my nick is
`jackdaniel`).

Sincerely yours,  
Daniel Kochmański
