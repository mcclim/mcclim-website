;;;;;
title: Crowdfunding McCLIM maintenance and development
date: 2016-08-12 15:00
format: md
author: Robert Strandh
;;;;;

McCLIM is currently the only free native toolkit for developing
GUI applications in Common Lisp.  A while ago, I took over the
maintenance, because I did not want to see McCLIM abandoned.

But since I have many other projects going, I decided to hire Daniel
Kochmański part time (25%) to do some of the urgent work.  This
experience turned out so well that I would like for him to continue
this work.  Not only am I very pleased with the quality of Daniel's
work, but I was also very surprised about the additional excitement
that his work generated.

While I could continue funding Daniel myself for a while, I can not do
so indefinitely.  For that reason, I decided that we should try
crowdfunding.  Steady funding at the 25% level for a year or more
would allow us to address some of the issues that often come up, such
as the numerous remaining defects, the lack of modern visual
appearance, and more.

If you are interested in seeing this happen, I strongly urge you to
consider contributing to this project.  Our target is modest.  We
would like to obtain at least 600 USD per month for at least one
year.  Even a small monthly contribution from a significant number of
people can make this happen.

To contribute, please visit
[https://salt.bountysource.com/teams/mcclim](https://salt.bountysource.com/teams/mcclim)
and follow the instructions.

Sincerely,

Robert Strandh
